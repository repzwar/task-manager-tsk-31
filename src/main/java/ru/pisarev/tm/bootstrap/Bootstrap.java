package ru.pisarev.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.ICommandRepository;
import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.repository.IUserRepository;
import ru.pisarev.tm.api.service.*;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.component.Backup;
import ru.pisarev.tm.component.FileScanner;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.system.UnknownCommandException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.repository.CommandRepository;
import ru.pisarev.tm.repository.ProjectRepository;
import ru.pisarev.tm.repository.TaskRepository;
import ru.pisarev.tm.repository.UserRepository;
import ru.pisarev.tm.service.*;
import ru.pisarev.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.pisarev.tm.util.SystemUtil.getPID;
import static ru.pisarev.tm.util.TerminalUtil.displayWait;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    public void start(String... args) {
        displayWelcome();
        initCommands();
        if (runArgs(args)) System.exit(0);
        process();
    }

    public void initApplication() {
        initPID();
        initData();
    }

    private void initData() {
        @NotNull final String adminId = userService.add("admin", "admin", "admin@a").getId();
        userService.findByLogin("admin").setRole(Role.ADMIN);
        @NotNull final String userId = userService.add("user", "user").getId();

        projectService.add(adminId, new Project("Project C", "-")).setStatus(Status.COMPLETED);
        projectService.add(adminId, new Project("Project A", "-"));
        projectService.add(adminId, new Project("Project B", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(adminId, new Project("Project D", "-")).setStatus(Status.COMPLETED);
        taskService.add(adminId, new Task("Task C", "-")).setStatus(Status.COMPLETED);
        taskService.add(adminId, new Task("Task A", "-"));
        taskService.add(adminId, new Task("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(adminId, new Task("Task D", "-")).setStatus(Status.COMPLETED);
        taskService.add(userId, new Task("Task B2", "-")).setStatus(Status.IN_PROGRESS);
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.pisarev.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.pisarev.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();

    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        authService.checkRoles(command.roles());
        command.execute();
        return true;
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        authService.checkRoles(abstractCommand.roles());
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";
        backup.init();
        fileScanner.init();
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

}
