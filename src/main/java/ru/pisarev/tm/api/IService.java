package ru.pisarev.tm.api;

import ru.pisarev.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}