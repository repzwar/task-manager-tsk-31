package ru.pisarev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.bootstrap.Bootstrap;
import ru.pisarev.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private static final int INTERVAL = 10;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final Bootstrap bootstrap;

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(
                bootstrap.getCommandService().getArguments().stream()
                        .map(AbstractCommand::name)
                        .collect(Collectors.toList())
        );
        es.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File("./");
        for (final File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String filename = item.getName();
            if (commands.contains(filename)) {
                bootstrap.runCommand(filename);
                item.delete();
            }
        }
    }

}
